package model;

import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;

public class UltraSonicSensorThreaded extends Thread{
	private EV3UltrasonicSensor ultrasonicSensor;
	
	public UltraSonicSensorThreaded(Port port) {
		this.ultrasonicSensor = new EV3UltrasonicSensor(port);
	}
	
	
	private SampleProvider detectDistance() {
		return this.ultrasonicSensor.getDistanceMode();
	}
	
 	public float getDistance() {
 		SampleProvider distance = ultrasonicSensor.getDistanceMode();
 		float[] sample = new float[distance.sampleSize()];
 		distance.fetchSample(sample, 0);
 		return sample[0];
 	}
	
	
	
	
}
