/**
 * 
 */
package model;

import java.util.Random;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.utility.Delay;

/**
 * @author csuser
 *
 */
public class MotorControlThreaded {

	private EV3LargeRegulatedMotor leftMotor;
	private EV3LargeRegulatedMotor rightMotor;

	public MotorControlThreaded(Port lMotor, Port rMotor) {

		this.leftMotor = new EV3LargeRegulatedMotor(lMotor);
		this.rightMotor = new EV3LargeRegulatedMotor(rMotor);

	}

	private EV3LargeRegulatedMotor reflexLeftRightCheck() {

		Random randomArbiter = new Random();

		if (randomArbiter.nextBoolean()) {
			return this.leftMotor;
		}
		return this.rightMotor;

	}

	private int reflexIntervalCheck() {

		Random randomArbiter = new Random();
		int randomInt = randomArbiter.nextInt(800);

		if (randomInt > 400) {
			return randomInt;
		}

		return 400;
	}

	public void moveForwardDelay(int msDelay) {

		this.leftMotor.forward();
		this.rightMotor.forward();
		Delay.msDelay(msDelay);

	}

	public void moveForward() {

		this.leftMotor.forward();
		this.rightMotor.forward();

	}

	public void turnLeft() {

		this.leftMotor.forward();

	}

	public void turnRight() {

		this.rightMotor.forward();

	}

	public void moveBackwards() {

		this.rightMotor.backward();
		this.leftMotor.backward();
		Delay.msDelay(750);

	}

	public void stop() {

		this.rightMotor.stop();
		this.leftMotor.stop();

	}

	public void reflexMoveForward() {

		this.rightMotor.forward();
		this.leftMotor.forward();
		Delay.msDelay(750);
		this.leftMotor.stop();
		this.rightMotor.stop();
		randomTurn();

	}

	public void reflexMoveBackward() {

		EV3LargeRegulatedMotor reflexMotor = this.reflexLeftRightCheck();
		reflexMotor.backward();
		Delay.msDelay(this.reflexIntervalCheck());
		reflexMotor.stop();

	}

	public void randomTurn() {

		this.leftMotor.stop();
		this.rightMotor.stop();
		EV3LargeRegulatedMotor reflexMotor = this.reflexLeftRightCheck();
		reflexMotor.forward();
		Delay.msDelay(this.reflexIntervalCheck());
		reflexMotor.stop();
	}

}
