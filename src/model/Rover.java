package model;

import controller.Controller;
import lejos.hardware.Button;
import lejos.hardware.port.Port;
import lejos.utility.Delay;
import lejos.hardware.Sound;

public class Rover {
	private MotorControlThreaded motorControl;
	private UltraSonicSensorThreaded ultrasonicControl;
	private ColorSensorThreaded colorControl;

	public Rover(Port colorPort, Port ultrasonicPort, Port rightMotor, Port leftMotor) {
		this.motorControl = new MotorControlThreaded(leftMotor, rightMotor);
		this.ultrasonicControl = new UltraSonicSensorThreaded(ultrasonicPort);
		this.colorControl = new ColorSensorThreaded(colorPort);
	}

	public void Run() {
		boolean dirtFlag = false;
		boolean baseFlag = false;
		boolean hasFoundBaseColor = false;
		boolean running = true;

		while (running && Button.ESCAPE.isUp()) {
			this.motorControl.moveForward();
			Delay.msDelay(500);
			while (hasFoundBaseColor == false && baseFlag == false && Button.ESCAPE.isUp()) {
				
				this.reflexMove();

				if (this.colorControl.searchForBaseColor()) {
					EV3ScreenLog.add("" + this.colorControl.getHomeBaseColorID());
					this.motorControl.moveForwardDelay(1000);

					hasFoundBaseColor = true;

				}

			}

			while (baseFlag == false && dirtFlag == false && Button.ESCAPE.isUp()) {

				reflexMove();

				if ((this.colorControl.SearchForDirt() == true)) {

					this.motorControl.stop();
					Sound.playTone(400, 1000);
					this.motorControl.moveForwardDelay(500);

					dirtFlag = true;

				} else if (this.colorControl.SearchForHomeBase() == true) {

					incorrectTapeTurn();
					this.motorControl.stop();

				}

				while (dirtFlag == true) {

					reflexMove();

					if (this.colorControl.SearchForHomeBase() == true) {

						this.motorControl.moveForwardDelay(1500);

						this.motorControl.stop();
						Sound.playTone(400, 1000);
						Delay.msDelay(100);
						Sound.playTone(400, 1000);
						this.motorControl.moveBackwards();
						Delay.msDelay(4000);
						this.motorControl.randomTurn();

						dirtFlag = false;
						baseFlag = false;

					}

				}

			}

		}
	}

	private void incorrectTapeTurn() {
		this.motorControl.moveBackwards();
		Delay.msDelay(500);
		this.motorControl.turnLeft();
		Delay.msDelay(1000);
	}

	private void reflexMove() {
		if (this.ultrasonicControl.getDistance() <= 0.20) {
			this.motorControl.reflexMoveBackward();
		} else {
			this.motorControl.moveForward();
		}

	}
}
