package model;

import java.util.ArrayList;

import lejos.hardware.lcd.LCD;

public class EV3ScreenLog {
	private static ArrayList<String> log = new ArrayList<String>();
	private static final int maxMessages = 6;

	/**
	 * Adds a message to the screen in a scrolling log.
	 * 
	 * @precondition The message is not null
	 * @postcondition The message is added to the lowest unoccupied row of the
	 *                screen. If the lowest row on the screen is occupied, every
	 *                existing message is moved up one (the top-most message is
	 *                removed) and the current message is added to the lowest
	 *                row.
	 * 
	 * @param message
	 *            The message to be added to the screen
	 */
	public static void add(String message) {
		EV3ScreenLog.checkForNullMessage(message);
		EV3ScreenLog.log.add(EV3ScreenLog.padMessage(message));
		EV3ScreenLog.checkForMaxMessageReached();
		EV3ScreenLog.redrawScreen();
	}

	public static void checkForNullMessage(String message) {
		if (message == null) {
			throw new NullPointerException("Message is null");
		}
	}

	public static void checkForMaxMessageReached() {
		if (EV3ScreenLog.log.size() > EV3ScreenLog.maxMessages) {
			EV3ScreenLog.log.remove(0);
		}
	}

	private static String padMessage(String message) {
		int neededPadding = 17 - message.length();

		for (int i = 0; i < neededPadding; i++) {
			message += " ";
		}

		return message;
	}

	private static void redrawScreen() {
		for (int i = 0; i < EV3ScreenLog.log.size(); i++) {
			LCD.drawString(EV3ScreenLog.log.get(i), 0, i);
		}
	}
}
