package model;

import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.Color;

public class ColorSensorThreaded extends Thread {

	private EV3ColorSensor colorSensor;
	private int homeBaseColorID;
	private int enemyBaseColorID;
	

	public ColorSensorThreaded(Port port) {
		this.colorSensor = new EV3ColorSensor(port);
	}
	
	public String printColor(){
		int colorID = this.colorSensor.getColorID();
		return Integer.toString(colorID);
	}

	public boolean SearchForDirt() {
		if (this.colorSensor.getColorID() == Color.BLACK) {
			return true;
		}
		return false;
	}
	
	
	public boolean searchForBaseColor() {
		if (this.colorSensor.getColorID() == Color.GREEN) {
			this.setHomeBaseColorID(1);
			this.enemyBaseColorID = 0;
			return true;
		} else if (this.colorSensor.getColorID() == Color.RED) {
			this.setHomeBaseColorID(0);
			this.enemyBaseColorID = 1;
			return true;
		}
		return false;
	}

	private void setHomeBaseColorID(int i) {
		this.homeBaseColorID = i;
		
	}

	public boolean SearchForHomeBase() {
		if (this.colorSensor.getColorID() == this.getHomeBaseColorID()) {
			return true;
		} 
		return false;
	}
	
	public boolean SearchForEnemyBase() {
		if (this.colorSensor.getColorID() == this.enemyBaseColorID) {
			return true;
		} 
		return false;
	}

	public int getHomeBaseColorID() {
		return homeBaseColorID;
	}

	

}
