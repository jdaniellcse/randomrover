package controller;

import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.utility.Delay;

import java.util.Random;

import lejos.hardware.Audio;
import model.EV3ScreenLog;
import model.Rover;

public class Controller {

	public static void main(String[] args) {
		EV3ScreenLog.add("Start the show!");

		Rover myRover = new Rover(SensorPort.S3, SensorPort.S2, MotorPort.A, MotorPort.B);
		
		myRover.Run();
	}
}
